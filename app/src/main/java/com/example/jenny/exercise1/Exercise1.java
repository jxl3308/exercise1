package com.example.jenny.exercise1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class Exercise1 extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise1);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercise1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnClick(View v) {
        EditText loanAmt = (EditText) findViewById(R.id.loanAmt); //get the loan text field
        double p = Double.parseDouble(loanAmt.getText().toString()); //change to string then to double

        // do same as above, except combined in one line
        double r = Double.parseDouble( ((EditText) findViewById(R.id.interestAmt)).getText().toString() );
        calculate(p, r);
    }

    //calculate the emi and the totals
    public void calculate(double p, double r) {
        if (r != 0) {
            r = r / 1200.0;
        }

        for (int n = 5; n <= 30; n=n+5)  {
            double emi = 0; //equated monthly installment
            double anotherN = n*12;
            //emi formula
            emi = (p * r * Math.pow((1 + r),anotherN)) / (Math.pow((1+r),anotherN) - 1);
            double total = emi * anotherN;

            //set text of "monthly installment"
            ((TextView)findViewById(R.id.monthlyAmt)).setText(String.format("$%.2f", emi));

            //set text for every 5 years up to 30
            switch(n) {
                case 5: {
                    TextView yr = (TextView) findViewById(R.id.fiveTotal);
                    yr.setText(String.format("$%.2f", total));
                    break;
                }
                case 10: {
                    TextView yr = (TextView) findViewById(R.id.tenTotal);
                    yr.setText(String.format("$%.2f", total));
                    break;
                }
                case 15: {
                    TextView yr = (TextView) findViewById(R.id.fifteenTotal);
                    yr.setText(String.format("$%.2f", total));
                    break;
                }
                case 20: {
                    TextView yr = (TextView) findViewById(R.id.twentyTotal);
                    yr.setText(String.format("$%.2f", total));
                    break;
                }
                case 25: {
                    TextView yr = (TextView) findViewById(R.id.twentyFiveTotal);
                    yr.setText(String.format("$%.2f", total));
                    break;
                }
                case 30: {
                    TextView yr = (TextView) findViewById(R.id.thirtyTotal);
                    yr.setText(String.format("$%.2f", total));
                    break;
                }
                default:break;
            }

        }
    }
}
